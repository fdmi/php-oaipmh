<?php
namespace Fdmi\Oaipmh\Client;
use GuzzleHttp\Client as GClient;

class ListRecords
{
    protected $url;
    protected $gurl;
    protected $gclient;
    protected $client;
    protected $myEndpoint;

    public function __construct($url)
    {
        $this->url = $url;
        $this->gurl = ['base_uri' => $this->url, 'timeout'  => 2.0, 'verify' => false ];
        $this->gclient = new GClient($this->gurl);
        $adapter = new \Phpoaipmh\HttpAdapter\CurlAdapter();
        $adapter->setCurlOpts([CURLOPT_TIMEOUT => 120]);
        $adapter->setCurlOpts([CURLOPT_SSL_VERIFYPEER => false]);
        $adapter->setCurlOpts([CURLOPT_SSL_VERIFYHOST => false]);
        $this->client = new \Phpoaipmh\Client($this->url, $adapter);
        $this->myEndpoint = new \Phpoaipmh\Endpoint($this->client);


    }

    public function getRecords($start=null, $end=null)
    {
        // Connection for list of records
        $mdate = array ();
        $mdate['start'] = $start;
        $mdate['end'] = $end;
        $v = new \Valitron\Validator($mdate);
        $v->rule('required', ['start', 'end']);
        $v->rule('date', 'start');
        $v->rule('date', 'end');
        #$v->rule('dateAfter', 'end', $mdate['start']);
        if ($v->validate()) {
            $start = new \DateTime($mdate['start']);
            $end = new \DateTime($mdate['end']);
        }
        return $this->myEndpoint->listRecords('oai_dc', $start, $end);
    }

    public function getRecord($rec)
    {
        $status = 'updated';
        $header = (array) $rec->header; #var_dump($header['@attributes']['status']);
        
        $finres = array();
        $finres['header'] = $header;
        if (isset($header['@attributes']['status'])) {
            if ($header['@attributes']['status'] == 'deleted') {
                $finres['header']['status'] = 'deleted';
            } else {
                $finres['header']['status'] = 'updated';
            }
        } else {
            $finres['header']['status'] = 'updated';
        }

        $finres['metadata'] = array();
        if ($finres['header']['status'] != 'deleted') {
            $data = $this->myEndpoint->getRecord($finres['header']['identifier'], 'oai_dc');
            $qarr['query']['verb'] = 'GetRecord';
            $qarr['query']['metadataPrefix'] = 'oai_dc';
            $qarr['query']['identifier'] = $finres['header']['identifier'];
            $gresponse = $this->gclient->request('GET', $this->url, $qarr);
            $xml = simplexml_load_string($gresponse->getBody(), NULL, NULL, "http://www.openarchives.org/OAI/2.0/");
            $_xml = (array) $xml; # Added for support UIN Suka OPAC
            #var_dump($_xml); die();
            if (isset($_xml['GetRecord'])) { # eprints, dspace, slims
              $metadata01 = $xml->GetRecord->record->metadata->children('http://www.openarchives.org/OAI/2.0/oai_dc/');
            } elseif (isset($_xml['ListRecords'])) { # UIN suka OPAC
              $metadata01 = $xml->ListRecords->record->metadata->children('http://www.openarchives.org/OAI/2.0/oai_dc/');
            }
            $metadata02 = $metadata01->children('http://purl.org/dc/elements/1.1/');
            $metadata02 = (array) $metadata02;
            $finres['metadata'] = $metadata02;
            // RESPONSE DATE
            $finres['header']['response_date'] = (string) $xml->responseDate; // assigned to res array
            // OAI URL
            $finres['header']['oai_url'] = (string) $xml->request; // assigned to res array
        }

        $tmpout = array();
        $tmpout['header'] = $finres['header'];
        $tmpout['metadata'] = $finres['metadata'];
        return json_encode($tmpout, JSON_PRETTY_PRINT);
    }

    # result this function is unpredictable. Usually unknown if retrieved records is so many
    public function count($resource) {
        return $resource->getTotalRecordCount() ?: 'unknown';
    }

}
