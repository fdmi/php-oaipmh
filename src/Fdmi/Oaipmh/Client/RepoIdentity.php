<?php
namespace Fdmi\Oaipmh\Client;

class RepoIdentity
{
    protected $url;
    protected $client;
    protected $myEndpoint;

    public function __construct($url)
    {
        $this->url = $url;
        $adapter = new \Phpoaipmh\HttpAdapter\CurlAdapter();
        $adapter->setCurlOpts([CURLOPT_TIMEOUT => 120]);
        $adapter->setCurlOpts([CURLOPT_SSL_VERIFYPEER => false]);
        $adapter->setCurlOpts([CURLOPT_SSL_VERIFYHOST => false]);
        $this->client = new \Phpoaipmh\Client($this->url, $adapter);
        $this->myEndpoint = new \Phpoaipmh\Endpoint($this->client);
    }

    public function getIdentity()
    {
        return $this->myEndpoint->identify();
    }

}