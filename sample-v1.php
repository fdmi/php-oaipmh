<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client as GClient;

$url = 'http://eprints.ipdn.ac.id/cgi/oai2'; #Eprints
#$url = 'https://pustaka.kemdikbud.go.id/libdikbud/oai2.php'; #slims
#$url = 'http://localhost/demo/01/oai2.php';
#$url = 'http://pustaka-digital.kemdikbud.go.id/demo/01/oai2.php';
#$url = 'http://demo.dspace.org/oai/request'; #dspace
$url = 'https://digilib.uin-suka.ac.id/cgi/oai2'; #Eprints
#$url = 'https://opac.uin-suka.ac.id/oai-pmh/public/';
$url = 'https://tulis.uinjkt.ac.id/oai/provider';
echo 'Harvesting from '.$url." ...\n";

$identify = new \Fdmi\Oaipmh\Client\RepoIdentity($url);
$identity = $identify->getIdentity();

echo 'Nama Repositori: '.$identity->Identify->repositoryName."\n"; sleep(1);

$list_records = new \Fdmi\Oaipmh\Client\ListRecords($url);

# To get all records
$recs = $list_records->getRecords();
# To records from date to date
#$recs = $list_records->getRecords('2022-01-02', '2022-01-05');

# result this function is unpredictable. Usually unknown if retrieved records is so many
echo "Total count is " .$list_records->count($recs)."\n";

foreach($recs as $rec) {
    echo $list_records->getRecord($rec);
}
